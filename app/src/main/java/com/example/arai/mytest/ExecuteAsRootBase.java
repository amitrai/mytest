package com.example.arai.mytest;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by arai on 30-08-2017.
 * Class to construct identify the su command to be used to execute the command.
 * as per our REQ, we need to copy a file from location which is not accessible with normal user permission.
 */

public abstract class ExecuteAsRootBase {
    private static final String TAG = "ExecuteAsRootBase";


    public final boolean execute() {
        boolean retval = false;

        try {
            ArrayList<String> commands = getCommandsToExecute();
            if (null != commands && commands.size() > 0) {
                Process suProcess = Runtime.getRuntime().exec("su");

                DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
                // Execute commands that require root access
                for (String currCommand : commands) {
                    os.writeBytes(currCommand + "\n");
                    os.flush();
                }
                os.writeBytes("exit\n");
                os.flush();
                try {
                    int suProcessRetval = suProcess.waitFor();
                    if (255 != suProcessRetval) {
                        // Root access granted
                        retval = true;
                    } else {
                        // Root access denied
                        retval = false;
                    }
                } catch (Exception ex) {
                    Log.e(TAG, "Error executing root action", ex);
                }
            }
        } catch (IOException ex) {
            Log.w(TAG, "Can't get root access");
        } catch (SecurityException ex) {
            Log.w(TAG, "Can't get root access", ex);
        } catch (Exception ex) {
            Log.w(TAG, "Error executing internal operation", ex);
        }
        return retval;
    }

    abstract ArrayList<String> getCommandsToExecute();
}
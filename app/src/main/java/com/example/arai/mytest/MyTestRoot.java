package com.example.arai.mytest;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arai on 30-08-2017.
 * Class to construct the su command for copying file.
 * as per our REQ, we need to copy a file from location which is not accessible with normal user permission.
 * in this example we are assuming to copy default.prop from / root folder of device internal storage to memory card root path.
 */

public class MyTestRoot extends ExecuteAsRootBase {

    private static final String TAG = "MyTestRoot";

    ArrayList<String> getCommandsToExecute() {
        ArrayList<String> cmdList = new ArrayList<String>();

        cmdList.add(generateCmd());

        return cmdList;
    }

    // assuming that we are trying to copy default.prop file from device root path to external memory card root path.
    private String generateCmd() {

        String cmd = "cp -r "; // which cmd to execute i.e copy
        String filetoCopy = "/default.prop";  // from path
        List<StorageUtils.StorageInfo> s = StorageUtils.getStorageList(); // get all available storage.

        // check for only external memory card.
        for (StorageUtils.StorageInfo sinfo : s) {
            if (sinfo.getDisplayName().contains("EXTERNAL")) {
                cmd = cmd + filetoCopy + " " + sinfo.path + filetoCopy;
            }
        }
        Log.d(TAG, "Command to File: " + cmd + "\n");
        return cmd;

    }

    // this function is used to evaluate SDK way to execute su command.
    public void testSDK() {
        //@TODO we can show error message if needed.
        execute();
    }

    // this function is used to evaluate NDK  way to execute su command.
    public void testNDK() {
        String str = "su -c " + generateCmd();
        //@TODO we can show error message if needed.
        checkRootAccessMethod(str);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native int checkRootAccessMethod(String cmdString);

}

#include <jni.h>
#include <string>
#include <stdlib.h>
#include <android/log.h>

#define  LOG_TAG    "NDKROOT"


#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)


extern "C"
JNIEXPORT jint JNICALL
Java_com_example_arai_mytest_MyTestRoot_checkRootAccessMethod(
        JNIEnv *env, jobject thiz, jstring cmdString) {
    //which command is enabled only after Busy box is installed on a rooted device
    //Output of which command is the path to su file. On a non rooted device , we will get a null/ empty path
    //char* cmd = const_cast<char *>"which su";
    FILE *pipe = popen("which su", "r");
    if (!pipe) return -1;
    char buffer[128];
    std::string resultCmd = "";
    while (!feof(pipe)) {
        if (fgets(buffer, 128, pipe) != NULL)
            resultCmd += buffer;
    }
    pclose(pipe);

    const char *cstr = resultCmd.c_str();
    int result = -1;
    if (cstr == NULL || (strlen(cstr) == 0)) {
        LOGD("Result of Which command is Null");
    } else {
        LOGD("Result of Which command %s\n", cstr);
        const char *nativeString = env->GetStringUTFChars(cmdString, 0);
        LOGD("Command to run-->%s", nativeString);
        env->ReleaseStringUTFChars(cmdString, nativeString);
        system(nativeString);
        result = 1;
    }
    return result;

}


